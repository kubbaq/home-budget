import 'reflect-metadata';
import express, { Response } from 'express';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import { InversifyExpressServer } from 'inversify-express-utils';
import { Container } from 'inversify';
import helmet from 'helmet';

import './controllers/health-check.controller';

class App {

  public server: InversifyExpressServer;

  constructor() {
    const container = new Container();
    this.server = new InversifyExpressServer(container);
    this.config();
  }

  private config(): void {
    dotenv.config();

    this.server.setConfig((app) => {
      app.use(bodyParser.json());
      app.use(bodyParser.urlencoded({ extended: false }));
      app.use(helmet());
    });

    this.server.setErrorConfig((app) => {
      app.use((err: Error, req:express.Request, res: Response, next: express.NextFunction) => {
        console.error(err.stack);
        res.status(500).send('Something broke!');
      });
    });
  }

}

export default new App().server.build();
