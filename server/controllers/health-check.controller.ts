import { controller, interfaces, httpGet } from 'inversify-express-utils';
import { Request, Response, NextFunction } from 'express';

@controller('/health-check')
export class HealthCheckController implements interfaces.Controller {
  @httpGet('/')
  private index(req: Request, res: Response, next: NextFunction): string {
    return 'Health - OK';
  }
}
